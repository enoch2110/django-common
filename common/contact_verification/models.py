# coding=utf-8
from __future__ import unicode_literals
from random import randint
from django.db import models

from .managers import PinQuerySet


class Pin(models.Model):
    contact = models.CharField(max_length=100)
    code = models.CharField(max_length=10)
    datetime = models.DateTimeField(auto_now_add=True)
    is_verified = models.BooleanField(default=False)
    objects = PinQuerySet.as_manager()

    class Meta:
        verbose_name = "문자인증내역"
        verbose_name_plural = "문자인증내역"

    def __unicode__(self):
        return "%s %s" % (self.contact, self.code)

    @staticmethod
    def is_verified_number(number):
        return Pin.objects.filter(phone_number=number, is_verified=True).exists()

    @staticmethod
    def generate_code():
        return str(randint(10000, 99999))

    def is_awaiting(self):
        return Pin.objects.awaiting().filter(id=self.id).exists()

    def save(self, *args, **kwargs):
        if not self.pk:
            verification_in_progress = Pin.objects.awaiting().filter(contact=self.contact).exists()
            if verification_in_progress:
                return
            else:
                self.code = Pin.generate_code()
        super(Pin, self).save(*args, **kwargs)

