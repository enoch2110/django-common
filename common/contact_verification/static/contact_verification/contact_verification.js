(function ($) {

    $.fn.contactVerification = function(options) {
        var settings = $.extend({
            sendButtonSelector: "#id_contact_button",
            verifyButtonSelector: "#id_code_button",
            onSendSuccess: null,
            onVerificationSuccess: null,
            contactSelector: "#id_contact",
            countrySelector: null,
            validator: null,
            method: 'sms'
        }, options );

        var $send_button = $(settings.sendButtonSelector);
        var $verify_button = $(settings.verifyButtonSelector);
        var $code_input = $(this);


        $send_button.click(function(){
            var contact = $(settings.contactSelector).val();

            if (!contact){
                alert("연락처를 입력하세요.");
                return;
            }

            if (settings.countrySelector){
                var country = $(settings.countrySelector).val();
                if (!country){
                    alert("Please select a country.");
                    return;
                }
                contact = "(" + country + ")" + contact;
            }

            $.ajax({
                url: '/contact_verification/pin/create/',
                method: 'post',
                data: {
                    contact: contact,
                    method: settings.method,
                    validator: settings.validator
                }
            }).done(function(data) {
                alert(data.message);
                if (data.result == 1 && settings.onSendSuccess){
                    settings.onSendSuccess();
                }
            }).fail(function(data) {
                alert("문자인증 중 문제가 발생했습니다.");
            })
        });

        $verify_button.click(function(){
            var contact = $(settings.contactSelector).val();
            var code = $code_input.val();

            if (!code){
                alert("인증코드를 입력하세요.");
                return;
            }

            $.ajax({
                url: '/contact_verification/pin/verify/',
                method: 'post',
                data: {
                    contact: contact,
                    code: code,
                    validator: settings.validator
                }
            }).done(function(data) {
                alert(data.message);
                if (data.result == 1 && settings.onVerificationSuccess){
                    settings.onVerificationSuccess();
                }
            }).fail(function(data) {
                alert("문자인증 중 문제가 발생했습니다.");
            })
        });
    };
}( jQuery ));