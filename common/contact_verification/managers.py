from django.db import models
import datetime


class PinQuerySet(models.QuerySet):
    def awaiting(self):
        expiry_datetime = datetime.datetime.now()-datetime.timedelta(minutes=3)
        return self.filter(datetime__gte=expiry_datetime, is_verified=False)

    def inactive(self):
        expiry_datetime = datetime.datetime.now()-datetime.timedelta(minutes=3)
        return self.filter(datetime__lt=expiry_datetime, is_verified=False)
