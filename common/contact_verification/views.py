# coding=utf-8
from __future__ import unicode_literals
import datetime

from django.http import JsonResponse
from django.utils.safestring import mark_safe
from django.views.decorators.csrf import csrf_exempt
from django.views.generic import View
from rest_framework import parsers, renderers
from rest_framework.views import APIView

from common.contact_verification import settings
from common.contact_verification.providers.coolsms import send_sms
from common.contact_verification.validators import ContactValidator
from .models import Pin


def load_class(name):
    components = name.split('.')
    mod = __import__(components[0])
    for comp in components[1:]:
        mod = getattr(mod, comp)
    return mod


class PinCreate(APIView):
    """
    result:
    1 success
    2 already sent
    3 send failed
    4 error
    """

    throttle_classes = ()
    permission_classes = ()
    parser_classes = (parsers.FormParser, parsers.MultiPartParser, parsers.JSONParser,)
    renderer_classes = (renderers.JSONRenderer,)

    def post(self, request, *args, **kwargs):
        now = datetime.datetime.now()
        contact = request.data.get('contact')
        validators = [ContactValidator, load_class(request.data.get('validator'))]
        messages = []
        for validator in validators:
            try:
                validator()(contact)
            except Exception as e:
                messages.append('<br>'.join(e))

        if messages:
            return JsonResponse({"result": 4, "message": mark_safe('<br>'.join(messages))})

        pin, created = Pin.objects.get_or_create(contact=contact, defaults={'is_verified': False})

        if pin.is_awaiting() and not created:
            remaining = (datetime.timedelta(minutes=3)-(now - pin.datetime)).seconds
            message = "인증코드가 이미 전송되었습니다. {0}초 후에 재발송 가능합니다.".format(remaining)
            result = 2
        else:
            if not created:
                pin.code = Pin.generate_code()
                pin.datetime = now
                pin.is_verified = False
                pin.save()

            message = settings.CONTACT_VERIFICATION_SMS_TEXT.format(code=pin.code)
            is_success = send_sms(message, str(contact), str(settings.CONTACT_VERIFICATION_SENDER))

            if is_success:
                result = 1
                message = "{0}로 인증코드를 전송하였습니다.".format(contact)
                remaining = 180
            else:
                pin.delete()
                result = 3
                message = "인증코드 전송을 실패하였습니다."
                remaining = None

        return JsonResponse({"result": result, "remaining": remaining, "message": message})

    @csrf_exempt
    def dispatch(self, request,  *args, **kwargs):
        return super(PinCreate, self).dispatch(request, *args, **kwargs)


class PinVerify(View):
    """
    result:
    1 success
    2 code expired
    3 incorrect code
    4 error
    """

    def post(self, request, *args, **kwargs):
        contact = request.POST.get('contact')
        code = request.POST.get('code')

        validator = load_class(request.POST.get('validator'))
        try:
            validator()(contact)
        except Exception as e:
            return JsonResponse({"result": 4, "message": ', '.join(e)})

        if Pin.objects.awaiting().filter(code=code, contact=contact).exists():
            result = 1
            message = "인증되었습니다."
        elif Pin.objects.filter(code=code, contact=contact).exists():
            result = 2
            message = "인증코드가 만료되었습니다."
        else:
            result = 3
            message = "올바른 인증코드가 아닙니다."

        return JsonResponse({"result": result, "message": message})

    @csrf_exempt
    def dispatch(self, request,  *args, **kwargs):
        return super(PinVerify, self).dispatch(request, *args, **kwargs)
