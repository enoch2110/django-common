from django.conf.urls import url
from common.blog import views

urlpatterns = [
    url(r'^create/(?P<category_id>\d+)$', views.EntryCreate.as_view(), name="entry-create"),
    url(r'^update/(?P<category_id>\d+)/(?P<pk>\d+)$', views.EntryUpdate.as_view(), name="entry-update"),
    url(r'^delete/(?P<category_id>\d+)/(?P<pk>\d+)$', views.EntryDelete.as_view(), name="entry-delete"),
    url(r'^commments/delete/(?P<pk>\d+)$', views.EntryCommentDelete.as_view(), name="entry-comment-delete"),
    url(r'^(?P<year>\d{4})/(?P<month>\d{2})/(?P<day>\d{2})/(?P<slug>[-\w]+)/$', views.EntryDetail.as_view(), name='entry_detail'),
]
