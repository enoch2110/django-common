"""Defaults urls for the Zinnia project"""
from django.conf.urls import url
from django.conf.urls import include

from django.utils.translation import ugettext_lazy

from zinnia.settings import TRANSLATED_URLS


def i18n_url(url, translate=TRANSLATED_URLS):
    """
    Translate or not an URL part.
    """
    if translate:
        return ugettext_lazy(url)
    return url

_ = i18n_url

urlpatterns = [
    url(_(r'^categories/'), include('common.blog.urls.categories')),
    url(_(r'^'), include('common.blog.urls.entries')),
]
