# coding=utf-8
from __future__ import unicode_literals
from django.utils.translation import ugettext_lazy as _
from ckeditor_uploader.widgets import CKEditorUploadingWidget
from django import forms
from django.conf import settings
from django.contrib.contenttypes.models import ContentType
from django.contrib.sites.models import Site
from django.utils import timezone
from django.utils.encoding import force_text
from django_comments.forms import CommentForm as CommentFormParent
from tagging.models import Tag
from zinnia.admin.fields import MPTTModelMultipleChoiceField
from zinnia.admin.forms import EntryAdminForm as EntryAdminFormParent
from zinnia.admin.widgets import MPTTFilteredSelectMultiple
from zinnia.models import Entry, Category
from zinnia.models.author import Author


class EntryAdminForm(EntryAdminFormParent):
    categories = MPTTModelMultipleChoiceField(
        label=_('Categories'), required=True, help_text="한 개의 카테고리만 지정하실 수 있습니다.",
        queryset=Category.objects.filter(children__isnull=True),
        widget=MPTTFilteredSelectMultiple(_('categories')))

    def clean_categories(self):
        data = self.cleaned_data['categories']
        if data.count() > 1:
            raise forms.ValidationError("한 개의 카테고리만 지정하실 수 있습니다.")
        return data

    def clean_tags(self):
        data = self.cleaned_data['tags']
        if len(data.split(",")) > 1:
            raise forms.ValidationError("한 개의 태그만 지정하실 수 있습니다.")
        return data

    def clean(self):
        cleaned_data = super(EntryAdminForm, self).clean()
        tags = cleaned_data.get("tags")
        categories = cleaned_data.get("categories")
        if categories and tags and not Tag.objects.get_for_object(categories.first()).filter(name=tags).exists():
            self.add_error('tags', '지정한 카테고리에 해당 태그를 넣을 수 없습니다')


class EntryForm(forms.ModelForm):
    tag = forms.ModelChoiceField(label='', queryset=Tag.objects.none())

    class Meta:
        model = Entry
        fields = ['tag', 'title', 'content', 'image', 'password', 'is_anonymous']
        labels = {
            'title': '제목',
            'content': '내용',
            'image': '메인이미지'
        }
        help_texts = {
            'image': '게시글 메인 이미지입니다.',
            'password': '비밀번호 설정시 본인과 운영자만 조회가 가능합니다.'
        }
        widgets = {
            'content': CKEditorUploadingWidget()
        }

    def __init__(self, *args, **kwargs):
        self.category = kwargs.pop('category', None)
        self.user = kwargs.pop('user', None)
        super(EntryForm, self).__init__(*args, **kwargs)
        tags = Tag.objects.get_for_object(self.category)
        if tags:
            self.fields['tag'].queryset = tags
        else:
            self.fields.pop('tag')

        if not self.category.info.allow_anonymous:
            self.fields.pop('is_anonymous')

        if not self.category.info.allow_password:
            self.fields.pop('password')

    def save(self, commit=True):
        instance = super(EntryForm, self).save(commit=False)
        instance.status = self.category.info.entry_default_status
        created = instance.pk
        tag = self.cleaned_data.get('tag')
        if commit:
            if tag:
                instance.tags = tag.name
            instance.save()
            if not created:
                instance.categories.add(self.category)
                instance.sites.add(Site.objects.get(id=settings.SITE_ID))
                if self.user.is_authenticated():
                    instance.authors.add(Author.objects.get(id=self.user.id))

        return instance


class EntryFilterForm(forms.Form):
    tag = forms.ModelChoiceField(label="카테고리", queryset=Tag.objects.none, required=False)
    search = forms.CharField(label="", required=False)

    def __init__(self, *args, **kwargs):
        category = kwargs.pop('category', None)
        super(EntryFilterForm, self).__init__(*args, **kwargs)
        tags = Tag.objects.get_for_object(category)
        if tags:
            self.fields['tag'].queryset = tags
        else:
            self.fields.pop('tag')


class CommentForm(CommentFormParent):
    is_anonymous = forms.BooleanField(label="익명댓글", required=False)
    name = None
    email = None
    url = None

    def get_comment_create_data(self):
        """
        Returns the dict of data to be used to create a comment. Subclasses in
        custom comment apps that override get_comment_model can override this
        method to add extra fields onto a custom comment model.
        """
        return dict(
            content_type=ContentType.objects.get_for_model(self.target_object),
            user_name="익명" if self.cleaned_data['is_anonymous'] else "",
            object_pk=force_text(self.target_object._get_pk_val()),
            comment=self.cleaned_data["comment"],
            submit_date=timezone.now(),
            site_id=settings.SITE_ID,
            is_public=True,
            is_removed=False,
        )
