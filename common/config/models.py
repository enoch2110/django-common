# coding=utf-8
from __future__ import unicode_literals

from django.contrib.contenttypes.fields import GenericForeignKey
from django.contrib.contenttypes.models import ContentType
from django.db import models
from mptt.fields import TreeForeignKey
from mptt.models import MPTTModel


class Configuration(MPTTModel):
    DATATYPE_CHOICES = [
        (1, 'short text'),
        (2, 'long text'),
        (3, 'rich text'),
        (4, 'file'),
        (5, 'model'),
    ]

    data_type = models.IntegerField("데이터형식", choices=DATATYPE_CHOICES)
    name = models.CharField("이름", max_length=200)
    category = models.CharField("카테고리", max_length=200)
    description = models.TextField("설명")
    file = models.FileField("파일")
    text = models.TextField("값")
    content_type = models.ForeignKey(ContentType, on_delete=models.CASCADE, verbose_name="컨텐트타입")
    content_object = GenericForeignKey('content_type', 'text')
    parent = TreeForeignKey('self', null=True, blank=True, related_name='children', db_index=True, verbose_name="상위 설정")

    class Meta:
        verbose_name = "설정"
        verbose_name_plural = "설정"

    @property
    def value(self):
        if self.data_type == 'FileInput':
            return self.file
        else:
            return self.text

    @staticmethod
    def __getitem__(name):
        return Configuration.objects.get(name=name).value()

