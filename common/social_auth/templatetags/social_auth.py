from __future__ import unicode_literals

import urllib

from django import template
from django.conf import settings
from django.utils.http import urlencode

register = template.Library()


@register.simple_tag(takes_context=True)
def social_auth_url(context, source):
    url = None

    context['request'].session['social_status'] = str(context['csrf_token'])
    if source == 'facebook':
        url = "https://www.facebook.com/dialog/oauth?client_id={client_id}&redirect_uri={redirect_uri}&scope=email&state={csrf}".format(
            client_id=settings.FACEBOOK_CLIENT_ID,
            redirect_uri=settings.FACEBOOK_REDIRECT_URI,
            csrf=context['csrf_token']
        )
    elif source == 'kakao':
        url = "https://kauth.kakao.com/oauth/authorize?client_id={client_id}&redirect_uri={redirect_uri}&response_type=code&state={csrf}".format(
            client_id=settings.KAKAO_CLIENT_ID,
            redirect_uri=settings.KAKAO_REDIRECT_URI,
            csrf=context['csrf_token']
        )
    elif source == 'naver':
        url = "https://nid.naver.com/oauth2.0/authorize?client_id={client_id}&response_type=code&redirect_uri={redirect_uri}&state={csrf}".format(
            client_id=settings.NAVER_CLIENT_ID,
            redirect_uri=settings.NAVER_REDIRECT_URI,
            csrf=context['csrf_token']
        )
    return url
